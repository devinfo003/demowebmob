import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  LogBox
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";
LogBox.ignoreLogs(['Warning: ...']); 
LogBox.ignoreAllLogs();
export default class UserListView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: {},
      connection_Status : ""
    };
  }
  componentDidMount() {
    NetInfo.addEventListener(this.handleConnectivityChange);
  }

  componentWillUnmount() {
    NetInfo.removeEventListener(this.handleConnectivityChange);
  }

  handleConnectivityChange = state => {
    if (state.isConnected) {
      this.fetchData();
    } else {
      this.dataRetrive();
    }
  }

  fetchData() {
    this.setState({ refreshing: true });
    fetch('https://randomuser.me/api/?inc=name,email,dob,phone,id&results=50')
      .then((res) => res.json())
      .then( async (resJson) => {
        console.log(resJson.results);
        this.setState({ data: resJson.results });
        this.setState({ refreshing: false });
        await AsyncStorage.setItem('UserData', JSON.stringify(resJson.results));
        // alert(this.state.data);
      })
      .catch((error) => console.log(error));
  }
 async dataRetrive(){
    try {
      const userlist = await AsyncStorage.getItem('UserData');
      if (userlist !== null){
        // We have data!!
        const userlist = await AsyncStorage.getItem('UserData');
        this.setState({ data: JSON.parse(await AsyncStorage.getItem('UserData')) });
      }
    } catch (error) {
      // Error retrieving data
    }
  }
 reformatDate(dateStr)
  {
    var dArr = dateStr.split("-");  // ex input "2020-01-18"
    return dArr[2]+ "/" +dArr[1]+ "/" +dArr[0].substring(2); //ex out: "18/01/20"
  }
  clickedUser(item){
    alert(item.item.name.title+" "+item.item.name.first+" "+item.item.name.last)
  }
  render() {
    return (
      <FlatList 
        enableEmptySections={true}
        data={this.state.data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={item => {
          return (
            <View style={styles.boxContainer}>
              <TouchableOpacity onPress={()=> this.clickedUser(item)} style={styles.box} >
                  <Image style={styles.image} source={{uri:'https://i.pinimg.com/originals/07/a5/72/07a5723295644be79473ff6dd554463b.png'}} />
                  <View style={styles.boxContent}>
                    <Text style={styles.title}>{item.item.name.title+" "+item.item.name.first+" "+item.item.name.last}</Text>
                    <Text style={styles.description}>{item.item.email}</Text>
                    <Text style={styles.description}>{this.reformatDate(item.item.dob.date.slice(0,9))}</Text>
                    <Text style={styles.description}>{item.item.phone}</Text>
                  </View>
              </TouchableOpacity>
            </View>
          )
        }}/>
    );
  }
}

const styles = StyleSheet.create({
  boxContainer:{
    backgroundColor:'#273443'
  },
  image: {
    width: 80,
    height:80,
    borderRadius:80/2,
  },
  box: {
    padding:20,
    marginTop:5,
    marginBottom:5,
    backgroundColor: '#000',
    flexDirection: 'row',
    borderRadius:10,
    alignSelf:'center',
    width:'95%',
    shadowColor: '#fff',
    shadowOffset: { width: 3, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 9,
  },
  boxContent: {
    flex:1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent:'center',
    marginLeft:10,
  },
  description:{
    fontSize:13,
    color: "#646464",
  },
  title:{
    fontSize:18,
    color:"#fff",
    opacity:0.8,
    fontWeight:'500'
  }
});